# [amazonka.net](https://amazonka.net) source codes

<br/>

### Run amazonka.net on localhost

    # vi /etc/systemd/system/amazonka.net.service

Insert code from amazonka.net.service

    # systemctl enable amazonka.net.service
    # systemctl start amazonka.net.service
    # systemctl status amazonka.net.service

http://localhost:4027
